#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file contains about all the functionnalities of the plugin
# but does not depend on XBMC, thus making it easily modifiable
# without knowledge of XBMC and causing no difficulty to use
# the features it provides elsewhere.

# Due to the fact that the content provided by RTS is by nature in French
# you should not be shocked that this source file may contain stuff in
# that language although code will be commented in English.

from bs4 import BeautifulSoup
from urllib2 import urlopen
from urllib import urlencode
import re
from datetime import date, timedelta

# A category is the name of a page containing a list of videos
# for instance "Temps présent" or "Les années 1930" or "Personnalité"
class Category(object):
    def __init__(self, title, url):
        self.title = title
        self.url = url

    def __repr__(self):
        return 'Category(%r, %r)' % (self.title, self.url)

    def str(self):
        return self.__repr__()


# A Video from the website
# On the category page it will contain only title, date, summary, duration, 
# media_type.
# TODO: On the player page, it will contain the rtmp info for streaming
class Video(object):
    title = None
    date = None
    summary = None
    description = None
    duration = None # timedelta
    media_type = None # MediaType.RADIO or MediaType.TV
    thumbnail_url = None
    page_url = None # video page url containing the player

    def __repr__(self):
        return 'Video(title=%r, date=%r, summary=%r, description=%r,\
 duration=%r, media_type=%r, thumbnail_url=%r,\
 page_url=%r)' % (self.title, self.date, self.summary,
                                 self.description, self.duration,
                                 self.media_type, self.thumbnail_url,
                                 self.page_url)

    def __str__(self):
        return self.__repr__() # or repr(self). Which is better style?


class MediaType(object):
    RADIO = 'radio'
    TV = 'tv'
    UNKNOWN = 'unknown'


# TODO A cache-enabled subclass for categories, using a @cache annotation
# on methods

class Scrapper(object):

    # TODO category_details(). Returns description ("Lancé le 18 avril 1969,
    # Temps Présent...") and number of shows. Logo URL would be nice too.

    # Returns a dictionary with 'Collections', 'Thématiques' and 'Index'
    # (these items appear on the website and are one of the main navigation
    # mean). Each of this item contain a list of Category() with a title/name
    # and the referenced link. This link can then be provided to 
    # self.video_page() to get the list of videos on that page.
    def categories(self):
        soup = self._make_soup('http://www.rts.ch/archives/')
        data = soup.select('.right-itm .level')

        # Categories as described in the top bar of the website
        categories = {
            u'Collections': self._category_menu_items(data[0]),
            # TODO Use cache for category long description to be shown
            # on XBMC category selection list.
            u'Thématiques': self._category_menu_items(data[1]),
            u'Index': self._category_menu_items(data[2]),
            # TODO Sport
        }
        # Decades
        categories[u'Décennies'] = []
        for decade in xrange(1930, 2000, 10):
            categories[u'Décennies'].append(
                Category(u'Années ' + str(decade),
                         'http://www.rts.ch/archives/decennie/1930/')
                )
        return categories

    # Returns a dictionary: {'videos': [Video()...], 
    # 'has_prev': bool, 'has_next': bool, 'page_id': integer, 
    # 'next_url': str}
    # The last four keys refer to the pagination.
    # 'next_url' is present only if 'has_next'
    def video_list_page(self, url):
        result = {}

        # Page ID is read from URL
        page_matcher = re.match('.*[&?]page=(\d)+.*', url)
        result['page_id'] = int(page_matcher.group(1)) if page_matcher else 1
        
        soup = self._make_soup(url)
        # Pagination
        page_elements = soup.select('.pages a')
        if page_elements:
            result['has_next'] = '>' in page_elements[-1].text
        else:
            result['has_next'] = False
        result['has_prev'] = result['page_id'] > 1
        if result['has_next']:
            result['next_url'] = self._next_video_list_page_url(url)

        result['videos'] = self.videos(url)
        return result

    def _make_soup(self, url):
        return BeautifulSoup(urlopen(url).read())

    # Returns a list of Video() from URL.
    def videos(self, url):
        results = []
        soup = self._make_soup(url)
        elements = soup.select('#videos ul li a')

        dt_prog = re.compile('(\d+).(\d+).(\d+)')
        duration_prog = re.compile(".*((\d+)'\s*(\d+)'').*", re.S)

        for e in elements:
            v = Video()
            # Title
            v.title = e.h4.text
            # URL of player
            v.page_url = 'http://www.rts.ch' + e['href']
            # URL of thumbnail
            v.thumbnail_url = 'http://www.rts.ch' + e.div.img['src']
            spans = e.find_all('span')
            v.summary = spans[1].text
            dt_matcher = dt_prog.match(spans[0].text)
            # Broadcast date
            v.date = date(int(dt_matcher.group(3)), 
                          int(dt_matcher.group(2)),
                          int(dt_matcher.group(1)))
            # TV or Radio?
            if spans[3].text == 'TV':
                v.media_type = MediaType.TV
            elif spans[3].text == 'RADIO':
                v.media_type = MediaType.RADIO
            else:
                v.media_type = MediaType.UNKNOWN
            # Duration
            dm = duration_prog.match(spans[2].contents[1])
            minutes = int(dm.group(2))
            seconds = int(dm.group(3))
            v.duration = timedelta(minutes=minutes, seconds=seconds)

            results.append(v)

        return results

    # PRE: 'has_next' (see video_list_page())
    def _next_video_list_page_url(self, current_url):
        matcher = re.match('.*([&?]page=(\d)+).*', current_url)
        if not matcher: # URL does not contain "... page=1234"
            return current_url + '&page=2'
        current_page_str = matcher.group(2) # '1', '2', ...
        # replace '123' in 'page=123' by '124'
        next_page_param = matcher.group(1).replace(
            current_page_str, str(int(current_page_str)+1))
        # replace 'page=123' with 'page=124' in full URL
        return matcher.group(0).replace(matcher.group(1), next_page_param)

    def _category_menu_items(self, elements):
        prog = re.compile('[\r\n\t]*(.*)')
        return [Category(
            prog.match(e.text).group(1), 
            'http://www.rts.ch' + e['href']
            ) for e in elements.find_all('a')]

    def search(self, keywords='', date_from='', date_to='', show='', 
               media='all'):
        url = 'http://www.rts.ch/archives/recherche/' + \
               urlencode({'keywords': keywords,
                          # TODO use datetime as args and strftime here
                          'from': date_from,
                          'to': date_to,
                          # TODO Autocomplete show name
                          'emission': show,
                          'media': media})
        return self.video_list_page(url)



if __name__ == '__main__':
    import pprint
    pretty_printer = pprint.PrettyPrinter(indent=4)
    pp = pretty_printer.pprint

    s = Scrapper()
    pp(s.video_list_page('http://www.rts.ch/archives/tv/information/temps-present/?integrales=true&page=2'))

# Example of page with description missing:
# http://www.rts.ch/archives/tv/information/temps-present/4134447-dominique-miracule-de-la-route.html
