#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), 'resources', 'lib'))


import xbmcplugin
import xbmcgui
from rts_scrapper import *
import urllib
import urlparse
import re

scrapper = Scrapper()

def add_primary_categories():
    [add_primary_category(category.encode('utf-8')) for category in scrapper.categories()]
    xbmcplugin.endOfDirectory(int(sys.argv[1]))

def add_primary_category(name):
    url = ''
    mode = sys.argv[1]
    icon_image = ''
    u = sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&primary="+urllib.quote_plus(name)
    print 'u', u
    item = xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=icon_image)
    item.setInfo( type='Video', infoLabels= { 'Title': name })
    return xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u, listitem=item, isFolder=True)

def add_secondary_category(primary_category, secondary_category):
    mode = sys.argv[1]
    icon_image = ''
    name = secondary_category.title
    u = sys.argv[0]+"?mode="+str(mode)+"&primary="+urllib.quote_plus(primary_category.encode('utf-8'))+\
        '&secondary='+urllib.quote_plus(name.encode('utf8'))+'&secondary_url='+\
        urllib.quote_plus(secondary_category.url.encode('utf-8'))
    item = xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=icon_image)
    item.setInfo( type='Video', infoLabels= { 'Title': secondary_category.title })
    return xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u, listitem=item, isFolder=True)

def add_secondary_categories(primary_category):
    [add_secondary_category(primary_category, sec) for sec in scrapper.categories()[primary_category]]
    xbmcplugin.endOfDirectory(int(sys.argv[1]))

def add_videos(url):
    [add_video(v) for v in scrapper.videos(url)]

def add_video(video):
    list_item = xbmcgui.ListItem(label=video.title, iconImage="DefaultVideo.png", 
        thumbnailImage=video.thumbnail_url,
        path='')


def get_params(query):
    ori = urlparse.parse_qs(query)
    # Convert {'bidule': ['chose']} to {'bidule': 'chose'}
    one_value_per_key_dict = {}
    for o in ori:
        one_value_per_key_dict[o] = ori[o][0]
    return one_value_per_key_dict

params = get_params(sys.argv[2][1:])

print 'Argv', str(sys.argv)

if not 'primary' in params:
    add_primary_categories()
else:
    if not 'secondary' in params:
        add_secondary_categories(params['primary'].decode('utf-8'))
    else:
        add_videos(params['secondary_url'])
